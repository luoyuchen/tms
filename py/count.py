import re
import json
pattern_html="<[a-z]+>|</[a-z]+>"
pattern_newline="\n"
pattern_sentence="\."
#pattern_word="[^a-zA-Z-]+"
pattern_disrupt="disrupted|disruption|disrupting|disrupts|disrupt|disruptive"
pattern_stimulate="stimulation|stimulate|stimulates|stimulated|stimulating|stimulus"
counts_disrupt=0
counts_stimulate=0

#load in data
papers=json.load(open('pnas.json'))
#extract words
for i,paper in enumerate(papers):
	abstract=paper["abstract"].encode("utf-8").lower()
	abstract=re.sub(pattern_html,"",abstract)
	abstract=re.sub(pattern_newline,"",abstract)
	sentences=re.split(pattern_sentence,abstract)
	for sentence in sentences:
		# if tms not found, ignore this sentence
		if sentence.find("tms")<0 and sentence.find("transcranial magnetic stimulation")<0:
			continue
		# otherwise count the target patterns in this sentence
		num=len(re.findall(pattern_disrupt,sentence))
		if num>0:
			#print i+1,num
			counts_disrupt+=num
		num=len(re.findall(pattern_stimulate,sentence))-len(re.findall("transcranial magnetic stimulation",sentence))
		if num>0:
			print i+1,num
			counts_stimulate+=num
print "number of papers scanned : "+str(len(papers))
print "disrupt in total : "+str(counts_disrupt)
print "stimulate in total : "+str(counts_stimulate)


