(Reward and motivation in pain and pain relief) <p>Pain is fundamentally unpleasant, a feature that protects the organism by promoting motivation and learning. Relief of aversive states, including pain, is rewarding. The aversiveness of pain, as well as the reward from relief of pain, is encoded by brain reward/motivational mesocorticolimbic circuitry. In this Review, we describe current knowledge of the impact of acute and chronic pain on reward/motivation circuits gained from preclinical models and from human neuroimaging. We highlight emerging clinical evidence suggesting that anatomical and functional changes in these circuits contribute to the transition from acute to chronic pain. We propose that assessing activity in these conserved circuits can offer new outcome measures for preclinical evaluation of analgesic efficacy to improve translation and speed drug discovery. We further suggest that targeting reward/motivation circuits may provide a path for normalizing the consequences of chronic pain to the brain, surpassing symptomatic management to promote recovery from chronic pain.</p>
(Simultaneous transcranial magnetic stimulation and single-neuron recording in alert non-human primates) <p>Transcranial magnetic stimulation (TMS) is a widely used, noninvasive method for stimulating nervous tissue, yet its mechanisms of effect are poorly understood. Here we report new methods for studying the influence of TMS on single neurons in the brain of alert non-human primates. We designed a TMS coil that focuses its effect near the tip of a recording electrode and recording electronics that enable direct acquisition of neuronal signals at the site of peak stimulus strength minimally perturbed by stimulation artifact in awake monkeys (<i>Macaca mulatta</i>). We recorded action potentials within ~1 ms after 0.4-ms TMS pulses and observed changes in activity that differed significantly for active stimulation as compared with sham stimulation. This methodology is compatible with standard equipment in primate laboratories, allowing easy implementation. Application of these tools will facilitate the refinement of next generation TMS devices, experiments and treatment protocols.</p>
(Lesions of prefrontal cortex reduce attentional modulation of neuronal responses and synchrony in V4) <p>It is widely held that the frontal eye field (FEF) in prefrontal cortex (PFC) modulates processing in visual cortex with attention, although the evidence that it is necessary is equivocal. To help identify critical sources of attentional feedback to area V4, we surgically removed the entire lateral PFC, including the FEF, in one hemisphere and transected the corpus callosum and anterior commissure in two macaques. This deprived V4 of PFC input in one hemisphere while keeping the other hemisphere intact. In the absence of PFC, attentional effects on neuronal responses and synchrony in V4 were substantially reduced and the remaining effects of attention were delayed in time, indicating a critical role for PFC. Conversely, distracters captured attention and influenced V4 responses. However, because the effects of attention in V4 were not eliminated by PFC lesions, other sources of top-down attentional control signals to visual cortex must exist outside of PFC.</p>
(Noninvasive brain stimulation: from physiology to network dynamics and back) <p>Noninvasive brain stimulation techniques have been widely used for studying the physiology of the CNS, identifying the functional role of specific brain structures and, more recently, exploring large-scale network dynamics. Here we review key findings that contribute to our understanding of the mechanisms underlying the physiological and behavioral effects of these techniques. We highlight recent innovations using <b>non</b>invasive stimulation to investigate global brain network dynamics and organization. New combinations of these techniques, in conjunction with neuroimaging, will further advance the utility of their application.</p>
(Dorsolateral and ventromedial prefrontal cortex orchestrate normative choice) <p>Humans are noted for their capacity to over-ride self-interest in favor of normatively valued goals. We examined the neural circuitry that is causally involved in normative, fairness-related decisions by generating a temporarily diminished capacity for costly normative behavior, a 'deviant' case, through non-invasive brain stimulation (repetitive transcranial magnetic stimulation) and compared normal subjects' functional magnetic resonance imaging signals with those of the deviant subjects. When fairness and economic self-interest were in conflict, normal subjects (who make costly normative decisions at a much higher frequency) displayed significantly higher activity in, and connectivity between, the right dorsolateral prefrontal cortex (DLPFC) and the posterior ventromedial prefrontal cortex (pVMPFC). In contrast, when there was no conflict between fairness and economic self-interest, both types of subjects displayed identical neural patterns and behaved identically. These findings suggest that a parsimonious prefrontal network, the activation of right DLPFC and pVMPFC, and the connectivity between them, facilitates subjects' willingness to incur the cost of normative decisions.</p>
(Causal role of the prefrontal cortex in top-down modulation of visual processing and working memory) <p>Selective attention filters information to limit what is encoded and maintained in working memory. Although the prefrontal cortex (PFC) is central to both selective attention and working memory, the underlying neural processes that link these cognitive abilities remain elusive. Using functional magnetic resonance imaging to guide repetitive transcranial magnetic stimulation with electroencephalographic recordings in humans, we perturbed PFC function at the inferior frontal junction in participants before they performed a selective-attention, delayed-recognition task. This resulted in diminished top-down modulation of activity in posterior cortex during early encoding stages, which predicted a subsequent decrement in working memory accuracy. Participants with stronger fronto-posterior functional connectivity displayed greater disruptive effects. Our data further suggests that broad alpha-band (7–14 Hz) phase coherence subserved this long-distance top-down modulation. These results suggest that top-down modulation mediated by the prefrontal cortex is a causal link between early attentional processes and subsequent memory performance.</p>
(Putting big data to good use in neuroscience) <p>Big data has transformed fields such as physics and genomics. Neuroscience is set to collect its own big data sets, but to exploit its full potential, there need to be ways to standardize, integrate and synthesize diverse types of data from different levels of analysis and across species. This will require a cultural shift in sharing data across labs, as well as to a central role for theorists in neuroscience research.</p>
(Transcranial focused ultrasound modulates the activity of primary somatosensory cortex in humans) <p>Improved methods of noninvasively modulating human brain function are needed. Here we probed the influence of transcranial focused ultrasound (tFUS) targeted to the human primary somatosensory cortex (S1) on sensory-evoked brain activity and sensory discrimination abilities. The lateral and axial spatial resolution of the tFUS beam implemented were 4.9 mm and 18 mm, respectively. Electroencephalographic recordings showed that tFUS significantly attenuated the amplitudes of somatosensory evoked potentials elicited by median nerve stimulation. We also found that tFUS significantly modulated the spectral content of sensory-evoked brain oscillations. The changes produced by tFUS on sensory-evoked brain activity were abolished when the acoustic beam was focused 1 cm anterior or posterior to S1. Behavioral investigations showed that tFUS targeted to S1 enhanced performance on sensory discrimination tasks without affecting task attention or response bias. We conclude that tFUS can be used to focally modulate human cortical function.</p>
(The roots of modern justice: cognitive and neural foundations of social norms and their enforcement) <p>Among animals, <i>Homo sapiens</i> is unique in its capacity for widespread cooperation and prosocial behavior among large and genetically heterogeneous groups of individuals. This ultra-sociality figures largely in our success as a species. It is also an enduring evolutionary mystery. There is considerable support for the hypothesis that this facility is a function of our ability to establish, and enforce through sanctions, social norms. Third-party punishment of norm violations (“I punish you because you harmed him”) seems especially crucial for the evolutionary stability of cooperation and is the cornerstone of modern systems of criminal justice. In this commentary, we outline some potential cognitive and neural processes that may underlie the ability to learn norms, to follow norms and to enforce norms through third-party punishment. We propose that such processes depend on several domain-general cognitive functions that have been repurposed, through evolution's thrift, to perform these roles.</p>
(Common medial frontal mechanisms of adaptive control in humans and rodents) <p>In this report we describe how common brain networks within the medial frontal cortex (MFC) facilitate adaptive behavioral control in rodents and humans. We demonstrate that after errors, low-frequency oscillations below 12 Hz are modulated over the midfrontal cortex in humans and within the prelimbic and anterior cingulate regions of the MFC in rats. These oscillations were phase locked between the MFC and motor areas in both rats and humans. In rats, single neurons that encoded prior behavioral outcomes were phase coherent with low-frequency field oscillations, particularly after errors. Inactivating the medial frontal regions in rats led to impaired behavioral adjustments after errors, eliminated the differential expression of low-frequency oscillations after errors and increased low-frequency spike-field coupling within the motor cortex. Our results describe a new mechanism for behavioral adaptation through low-frequency oscillations and elucidate how medial frontal networks synchronize brain activity to guide performance.</p>
(The effects of electrical microstimulation on cortical signal propagation) <p>Electrical stimulation has been used in animals and humans to study potential causal links between neural activity and specific cognitive functions. Recently, it has found increasing use in electrotherapy and neural prostheses. However, the manner in which electrical stimulation–elicited signals propagate in brain tissues remains unclear. We used combined electrostimulation, neurophysiology, microinjection and functional magnetic resonance imaging (fMRI) to study the cortical activity patterns elicited during stimulation of cortical afferents in monkeys. We found that stimulation of a site in the lateral geniculate nucleus (LGN) increased the fMRI signal in the regions of primary visual cortex (V1) that received input from that site, but suppressed it in the retinotopically matched regions of extrastriate cortex. Consistent with previous observations, intracranial recordings indicated that a short excitatory response occurring immediately after a stimulation pulse was followed by a long-lasting inhibition. Following microinjections of GABA antagonists in V1, LGN stimulation induced positive fMRI signals in all of the cortical areas. Taken together, our findings suggest that electrical stimulation disrupts cortico-cortical signal propagation by silencing the output of any neocortical area whose afferents are electrically stimulated.</p>
(A real red-letter day) <p class="lead">Synesthesia, in which letters or numbers elicit color perception, could be due to increased brain connectivity between relevant regions, or due to failure to inhibit feedback in cortical circuits. Diffusion tensor imaging now provides evidence for increased connectivity in word processing and binding regions of the brain.</p>
(Targeting abnormal neural circuits in mood and anxiety disorders: from the laboratory to the clinic) <p class="lead">Recent decades have witnessed tremendous advances in the neuroscience of emotion, learning and memory, and in animal models for understanding depression and anxiety. This review focuses on new rationally designed psychiatric treatments derived from preclinical human and animal studies. Nonpharmacological treatments that affect disrupted emotion circuits include vagal nerve stimulation, rapid transcranial magnetic stimulation and deep brain stimulation, all borrowed from neurological interventions that attempt to target known pathological foci. Other approaches include drugs that are given in relation to specific learning events to enhance or disrupt endogenous emotional learning processes. Imaging data suggest that common regions of brain activation are targeted with pharmacological and somatic treatments as well as with the emotional learning in psychotherapy. Although many of these approaches are experimental, the rapidly developing understanding of emotional circuit regulation is likely to provide exciting and powerful future treatments for debilitating mood and anxiety disorders.</p>
(Action anticipation and motor resonance in elite basketball players) <p class="lead">We combined psychophysical and transcranial magnetic stimulation studies to investigate the dynamics of action anticipation and its underlying neural correlates in professional basketball players. Athletes predicted the success of free shots at a basket earlier and more accurately than did individuals with comparable visual experience (coaches or sports journalists) and novices. Moreover, performance between athletes and the other groups differed before the ball was seen to leave the model's hands, suggesting that athletes predicted the basket shot's fate by reading the body kinematics. Both visuo-motor and visual experts showed a selective increase of motor-evoked potentials during observation of basket shots. However, only athletes showed a time-specific motor activation during observation of erroneous basket throws. Results suggest that achieving excellence in sports may be related to the fine-tuning of specific anticipatory 'resonance' mechanisms that endow elite athletes' brains with the ability to predict others' actions ahead of their realization.</p>
(Switching from automatic to controlled action by monkey medial frontal cortex) <p class="lead">Human behavior is mostly composed of habitual actions that require little conscious control. Such actions may become invalid if the environment changes, at which point individuals need to switch behavior by overcoming habitual actions that are otherwise triggered automatically. It is unknown how the brain controls this type of behavioral switching. Here we show that the presupplementary motor area (pre-SMA) in the medial frontal cortex has a function in switching from automatic to volitionally controlled action in rhesus macaque monkeys. We found that a group of pre-SMA neurons was selectively activated when subjects successfully switched to a controlled alternative action. Electrical stimulation in the pre-SMA replaced automatic incorrect responses with slower correct responses. A further test suggested that the pre-SMA enabled switching by first suppressing an automatic unwanted action and then boosting a controlled desired action. Our data suggest that the pre-SMA resolves response conflict so that the desired action can be selected.</p>
(Opposite biases in salience-based selection for the left and right posterior parietal cortex) <p class="lead">Visual selection is determined in part by the saliency of stimuli. We assessed the brain mechanisms determining attentional responses to saliency. Repetitive transcranial magnetic stimulation (rTMS) was applied to the left and right posterior parietal cortices (PPC) immediately before participants were asked to identify a compound letter. rTMS to the right PPC disrupted the guidance of attention toward salient stimuli, whereas rTMS to the left PPC affected the ability to bias selection away from salient stimuli. We conclude that right and left PPC have opposite roles in biasing selection to and from salient stimuli in the environment.</p>
(Arm immobilization causes cortical plastic changes and locally decreases sleep slow wave activity) <p class="lead">Sleep slow wave activity (SWA) is thought to reflect sleep need, increasing after wakefulness and decreasing after sleep. We showed recently that a learning task involving a circumscribed brain region produces a local increase in sleep SWA. We hypothesized that increases in cortical SWA reflect synaptic potentiation triggered by learning. To further investigate the link between synaptic plasticity and sleep, we asked whether a procedure leading to synaptic depression would cause instead a decrease in sleep SWA. We show here that if a subject's arm is immobilized during the day, motor performance deteriorates and both somatosensory and motor evoked potentials decrease over contralateral sensorimotor cortex, indicative of local synaptic depression. Notably, during subsequent sleep, SWA over the same cortical area is markedly reduced. Thus, cortical plasticity is linked to local sleep regulation without learning in the classical sense. Moreover, when synaptic strength is reduced, local sleep need is also reduced.</p>
(val66met polymorphism is associated with modified experience-dependent plasticity in human motor cortex) <p class="lead">Motor training can induce profound physiological plasticity within primary motor cortex, including changes in corticospinal output and motor map topography. Using transcranial magnetic stimulation, we show that training-dependent increases in the amplitude of motor-evoked potentials and motor map reorganization are reduced in healthy subjects with a val66met polymorphism in the brain-derived neurotrophic factor gene (<i>BDNF</i>), as compared to subjects without the polymorphism. The results suggest that BDNF is involved in mediating experience-dependent plasticity of human motor cortex.</p>
(Task-specific signal transmission from prefrontal cortex in visual selective attention) <p class="lead">Our voluntary behaviors are thought to be controlled by top-down signals from the prefrontal cortex that modulate neural processing in the posterior cortices according to the behavioral goal. However, we have insufficient evidence for the causal effect of the top-down signals. We applied a single-pulse transcranial magnetic stimulation over the human prefrontal cortex and measured the strength of the top-down signals as an increase in the efficiency of neural impulse transmission. The impulse induced by the stimulation transmitted to different posterior visual areas depending on the domain of visual features to which subjects attended. We also found that the amount of impulse transmission was associated with the level of attentional preparation and the performance of visual selective-attention tasks, consistent with the causal role of prefrontal top-down signals.</p>
(Neural basis and recovery of spatial attention deficits in spatial neglect) <p class="lead">The syndrome of spatial neglect is typically associated with focal injury to the temporoparietal or ventral frontal cortex. This syndrome shows spontaneous partial recovery, but the neural basis of both spatial neglect and its recovery is largely unknown. We show that spatial attention deficits in neglect (rightward bias and reorienting) after right frontal damage correlate with abnormal activation of structurally intact dorsal and ventral parietal regions that mediate related attentional operations in the normal brain. Furthermore, recovery of these attention deficits correlates with the restoration and rebalancing of activity within these regions. These results support a model of recovery based on the re-weighting of activity within a distributed neuronal architecture, and they show that behavioral deficits depend not only on structural changes at the locus of injury, but also on physiological changes in distant but functionally related brain areas.</p>
(The painful side of empathy) <p class="lead">Empathy refers to our ability to share emotions and sensations such as pain with others. Imaging studies on pain showed that the affective but not sensory component of our pain experience is involved in empathy for pain. In contrast, a new study using transcranial magnetic stimulation highlights for the first time the role of sensorimotor components in empathy for pain in other people.</p>
(Shift of activity from attention to motor-related brain areas during visual learning) <p class="lead">With practice, we become increasingly efficient at visual object comparisons. This may be due to the formation of a memory template that not only binds individual features together to create an object, but also links the object with an associated response. In a longitudinal fMRI study of object matching, evidence for this link between perception and action was observed as a shift of activation from visual-attentive processing areas along the posterior intraparietal sulcus to hand-sensory and motor-related areas.</p>
(Striate cortex (V1) activity gates awareness of motion) <p class="lead">A key question in understanding visual awareness is whether any single cortical area is indispensable. In a transcranial magnetic stimulation experiment, we show that observers' awareness of activity in extrastriate area V5 depends on the amount of activity in striate cortex (V1). From the timing and pattern of effects, we infer that back-projections from extrastriate cortex influence information content in V1, but it is V1 that determines whether that information reaches awareness.</p>
(Making the causal link: frontal cortex activity and repetition priming) <p class="lead">Object identification improves with repeated presentation, but neural activity decreases. In a new study, disrupting inferior frontal activity with transcranial magnetic stimulation during initial exposure to an object blocks later behavioral and neural changes.</p>
(Reductions in neural activity underlie behavioral components of repetition priming) <p class="lead">Repetition priming is a nonconscious form of memory that is accompanied by reductions in neural activity when an experience is repeated. To date, however, there is no direct evidence that these neural reductions underlie the behavioral advantage afforded to repeated material. Here we demonstrate a causal linkage between neural and behavioral priming in humans. fMRI (functional magnetic resonance imaging) was used in combination with transcranial magnetic stimulation (TMS) to target and disrupt activity in the left frontal cortex during repeated classification of objects. Left-frontal TMS disrupted both the neural and behavioral markers of priming. Neural priming in early sensory regions was unaffected by left-frontal TMS—a finding that provides evidence for separable conceptual and perceptual components of priming.</p>
(Sensorimotor attenuation by central motor command signals in the absence of movement) <p class="lead">Voluntary actions typically produce suppression of afferent sensation from the moving body part. We used transcranial magnetic stimulation to delay the output of motor commands from the motor cortex during voluntary movement. We show attenuation of sensation during this delay, in the absence of movement. We conclude that sensory suppression mainly relies on central signals related to the preparation for movement and that these signals are upstream of primary motor cortex.</p>
(Transcranial magnetic stimulation highlights the sensorimotor side of empathy for pain) <p class="lead">Pain is intimately linked with action systems that are involved in observational learning and imitation. Motor responses to one's own pain allow freezing or escape reactions and ultimately survival. Here we show that similar motor responses occur as a result of observation of painful events in others. We used transcranial magnetic stimulation to record changes in corticospinal motor representations of hand muscles of individuals observing needles penetrating hands or feet of a human model or noncorporeal objects. We found a reduction in amplitude of motor-evoked potentials that was specific to the muscle that subjects observed being pricked. This inhibition correlated with the observer's subjective rating of the sensory qualities of the pain attributed to the model and with sensory, but not emotional, state or trait empathy measures. The empathic inference about the sensory qualities of others' pain and their automatic embodiment in the observer's motor system may be crucial for the social learning of reactions to pain.</p>
(Virtual lesions of the anterior intraparietal area disrupt goal-dependent on-line adjustments of grasp) <p class="lead">Adaptive motor behavior requires efficient error detection and correction. The posterior parietal cortex is critical for on-line control of reach-to-grasp movements. Here we show a causal relationship between disruption of cortical activity within the anterior intraparietal sulcus (aIPS) by transcranial magnetic stimulation (TMS) and disruption of goal-directed prehensile actions (either grip size or forearm rotation, depending on the task goal, with reaching preserved in either case). Deficits were elicited by applying TMS within 65 ms after object perturbation, which attributes a rapid control process on the basis of visual feedback to aIPS. No aperture deficits were produced when TMS was applied to a more caudal region within the intraparietal sulcus, to the parieto-occipital complex (putative V6, V6A) or to the hand area of primary motor cortex. We contend that aIPS is critical for dynamic error detection during goal-dependent reach-to-grasp action that is visually guided.</p>
(Listening to speech activates motor areas involved in speech production) <b>To examine the role of motor areas in speech perception, we carried out a functional magnetic resonance imaging (fMRI) study in which subjects listened passively to monosyllables and produced the same speech sounds. Listening to speech activated bilaterally a superior portion of ventral premotor cortex that largely overlapped a speech production motor area centered just posteriorly on the border of Brodmann areas 4a and 6, which we distinguished from a more ventral speech production area centered in area 4p. Our findings support the view that the motor system is recruited in mapping acoustic inputs to a phonetic code.<br><br></b>
(Cortical hierarchy turned on its head) <b>Sighted people devote much their cortex to visual processing, but in the blind, 'visual' areas are recruited for other senses. A paper in this issue now reports that V1 in the blind is also activated by a verbal memory task, without any sensory input.</b>
(Attending to local form while ignoring global aspects depends on handedness: evidence from TMS) <b>Our perceptions of the whole and of the parts of a visual stimulus are mediated by different brain regions. We used low-frequency transcranial magnetic stimulation (TMS) to show for the first time that opposite, homologous regions in the two hemispheres are involved in attending to local parts for left- and right-handed individuals. The brain regions that focus on the 'trees' while ignoring the 'forest' are switched as a function of handedness.<br><br></b>
(Human fMRI evidence for the neural correlates of preparatory set) <b>We used functional magnetic resonance imaging (fMRI) to study readiness and intention signals in frontal and parietal areas that have been implicated in planning saccadic eye movementsthe frontal eye fields (FEF) and intraparietal sulcus (IPS). To track fMRI signal changes correlated with readiness to act, we used an event-related design with variable gap periods between disappearance of the fixation point and appearance of the target. To track changes associated with intention, subjects were instructed before the gap period to make either a pro-saccade (look at target) or an anti-saccade (look away from target). FEF activation increased during the gap period and was higher for anti- than for pro-saccade trials. No signal increases were observed during the gap period in the IPS. Our findings suggest that within the frontoparietal networks that control saccade generation, the human FEF, but not the IPS, is critically involved in preparatory set, coding both the readiness and intention to perform a particular movement.</b>
(Transcranial magnetic stimulation of the occipital pole interferes with verbal processing in blind subjects) <b>Recent neuroimaging studies in blind persons show that the occipital cortex, including the primary visual cortex (V1), is active during language-related and verbal-memory tasks. No studies, however, have identified a causal link between early visual cortex activity and successful performance on such tasks. We show here that repetitive transcranial magnetic stimulation (rTMS) of the occipital pole reduces accuracy on a verb-generation task in blind subjects, but not in sighted controls. An analysis of error types revealed that the most common error produced by rTMS was semantic; phonological errors and interference with motor execution or articulation were rare. Thus, in blind persons, a transient 'virtual lesion' of the left occipital cortex interferes with high-level verbal processing.</b>
(The site of saccadic suppression) <b>During rapid eye movements, or saccades, stable vision is maintained by active reduction of visual sensitivity. The site of this saccadic suppression remains uncertain. Here we show that phosphenessmall illusory visual perceptionsinduced by transcranial magnetic stimulation (TMS) to the human occipital cortex are immune to saccadic suppression, whereas phosphenes induced by retinal stimulation are not, thus providing direct physiological evidence that saccadic suppression occurs between the retina and the occipital visual cortex.<br><br></b>
(Magnetic stimulation reveals the distribution of language in a normal population) <b>Language is classically considered to be a function of the left side of the brain. Now an interference technique, transcranial magnetic stimulation, in healthy subjects shows that the right-side language activity detected in some people is indeed functionally relevant.</b>
(Preparatory activity in motor cortex reflects learning of local visuomotor skills) <b>In humans, learning to produce correct visually guided movements to adapt to new sensorimotor conditions requires the formation of an internal model that represents the new transformation between visual input and the required motor command. When the new environment requires adaptation to directional errors, learning generalizes poorly to untrained locations and directions, indicating that such learning is local. Here we replicated these behavioral findings in rhesus monkeys using a visuomotor rotation task and simultaneously recorded neuronal activity. Specific changes in activity were observed only in a subpopulation of cells in the motor cortex with directional properties corresponding to the locally learned rotation. These changes adhered to the dynamics of behavior during learning and persisted between learning and relearning of the same rotation. These findings suggest a neural mechanism for the locality of newly acquired sensorimotor tasks and provide electrophysiological evidence for their retention in working memory.</b>
(Degree of language lateralization determines susceptibility to unilateral brain lesions) <b>Language is considered a function of either the left or, in exceptional cases, the right side of the brain. Functional imaging studies show, however, that in the general population a graded continuum from left hemispheric to right hemispheric language lateralization exists. To determine the functional relevance of lateralization differences, we suppressed language regions using transcranial magnetic stimulation (TMS) in healthy human subjects who differed in lateralization of language-related brain activation. Language disruption correlated with both the degree and side of lateralization. Subjects with weak lateralization (more bilaterality) were less affected by either left- or right-side TMS than were subjects with strong lateralization to one hemisphere. Thus in some people, language processing seems to be distributed evenly between the hemispheres, allowing for ready compensation after a unilateral lesion.</b>
(Complementary localization and lateralization of orienting and motor attention) <b>4</b>
(Voluntary action and conscious awareness) <b>Humans have the conscious experience of 'free will': we feel we can generate our actions, and thus affect our environment. Here we used the perceived time of intentional actions and of their sensory consequences as a means to study consciousness of action. These perceived times were attracted together in conscious awareness, so that subjects perceived voluntary movements as occurring later and their sensory consequences as occurring earlier than they actually did. Comparable involuntary movements caused by magnetic brain stimulation reversed this attraction effect. We conclude that the CNS applies a specific neural mechanism to produce intentional binding of actions and their effects in conscious awareness.</b>
(Virtual neurology) <b>Using transcranial magnetic stimulation to induce a 'virtual lesion' in the parietal lobe, a new study reveals the mechanisms of hemispatial neglect, a neurological disorder of attention.</b>
(Fast and slow parietal pathways mediate spatial attention) <b>Mechanisms of selective attention are vital for guiding human behavior. The parietal cortex has long been recognized as a neural substrate of spatial attention, but the unique role of distinct parietal subregions has remained unclear. Using single-pulse transcranial magnetic stimulation, we found that the angular gyrus of the right parietal cortex mediates spatial orienting during two distinct time periods after the onset of a behaviorally relevant event. The biphasic involvement of the angular gyrus suggests that both fast and slow visual pathways are necessary for orienting spatial attention.<br><br></b>
(Complementary localization and lateralization of orienting and motor attention) <b>It is widely agreed that the right posterior parietal cortex has a preeminent role in visuospatial and orienting attention. A number of lines of evidence suggest that although orienting and the preparation of oculomotor responses are dissociable from each other, the two are intimately related. If this is true, then it should be possible to identify other attentional mechanisms tied to other response modalities. We used repetitive transcranial magnetic stimulation (rTMS) to demonstrate the existence of a distinct anterior parietal mechanism of motor attention. The critical area for motor attention is anterior to the one concerned with orienting, and it is lateralized to the left hemisphere in humans.</b>
(Emerging ethical issues in neuroscience) <b>There is growing public awareness of the ethical issues raised by progress in many areas of neuroscience. This commentary reviews the issues, which are triaged in terms of their novelty and their imminence, with an exploration of the relevant ethical principles in each case.</b>
(Transcranial magnetic stimulation of medial−frontal cortex impairs the processing of angry facial expressions) <b>Growing evidence suggests that the recognition of different emotional states involves at least partly separable neural circuits. Here we assessed the discrimination of both anger and happiness in healthy subjects receiving transcranial magnetic stimulation (TMS) over the medial−frontal cortex or over a control site (mid-line parietal cortex). We found that TMS over the medial−frontal cortex impairs the processing of angry, but not happy, facial expressions of emotion.<br><br></b>
(Enhanced visual spatial attention ipsilateral to rTMS-induced 'virtual lesions' of human parietal cortex) <b>The breakdown of attentional mechanisms after brain damage can have drastic behavioral consequences, as in patients suffering from spatial neglect. While much research has concentrated on impaired attention to targets contralateral to sites of brain damage, here we report the ipsilateral enhancement of visual attention after repetitive transcranial magnetic stimulation (rTMS) of parietal cortex at parameters known to reduce cortical excitability. Normal healthy subjects received rTMS (1 Hz, 10 mins) over right or left parietal cortex. Subsequently, detection of visual stimuli contralateral to the stimulated hemisphere was consistently impaired when stimuli were also present in the opposite hemifield, mirroring the extinction phenomenon commonly observed in neglect patients. Additionally, subjects' attention to ipsilateral targets improved significantly over normal levels. These results underline the potential of focal brain dysfunction to produce behavioral improvement and give experimental support to models of interhemispheric competition in the distributed brain network for spatial attention.</b>
(Prefontal cortex in long-term memory: an "interference" approach using magnetic stimulation) <b>Neuroimaging has consistently shown engagement of the prefrontal cortex during episodic memory tasks, but the functional relevance of this metabolic/hemodynamic activation in memory processing is still to be determined. We used repetitive transcranial magnetic stimulation (rTMS) to transiently interfere with either left or right prefrontal brain activity during the encoding or retrieval of pictures showing complex scenes. We found that the right dorsolateral prefrontal cortex (DLPFC) was crucial for the retrieval of the encoded pictorial information, whereas the left DLPFC was involved in encoding operations. This 'interference' approach allowed us to establish whether a cortical area activated by a memory task actually contributes to behavioral performance.</b>
